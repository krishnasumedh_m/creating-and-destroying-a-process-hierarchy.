Repository Details : 

* Quick summary -->
  -Involves creation and termination of processes.
  -Used the system calls fork(), exec(), wait() to recursively create a process hierarchy tree that prints 
   out the process tree information as the tree is being created, and then systematically terminate all the processes in the tree.
* Version
   1.0
