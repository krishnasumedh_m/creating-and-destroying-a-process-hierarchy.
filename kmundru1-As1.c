#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>  //execlp()
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
void indentation(int sp);// function used for Indendation

if(argc != 3) // check for argc value
{
      fprintf(stderr, "Error: invalid arguments.\n\n");
    	printf("Usage: ./exname [arg1->H] [arg2->C] \n");
    	exit(1);       
}

int i=0; 
int status;
pid_t pid;
int H=0;
int C=0;
char ht[40];
H = atoi(argv[1]);
C = atoi(argv[2]);
  indentation(H);
printf("Process starting \n");
  indentation(H);
printf("Parent's ID = %d \n", getppid());
  indentation(H);
printf("Height in the tree = %d \n", H) ;  

if(H > 1 )
{
indentation(H);
printf("Creating %d children at height %d \n", C, H-1);
  
for(i=1;i<=C;i++)
{ 
 
int cpid;
cpid=fork();

		if (cpid  < 0) 
		{			
			printf("Error while creating child %d\n", i);
			exit(3);
		}
		
		if( cpid == 0 ) 
		{		         
			   sprintf(ht, "%d", H-1);
			   execlp(argv[0], argv[0],ht,argv[2], (char *)NULL);
                     indentation(H);
			   printf("Error while performing execlp in child %d\n", i);
			   exit(4);
		}		
                 
}// end of for()

 for( i=0; i<C; i++) 
	{
		int a=wait(&status);
	}
}
// end of if(H>1)

 indentation(H);
 printf("Terminating at height %d \n", H);
return 0;
}  // end of main()


void indentation(int sp)
{
int i;
    printf("(%d)",getpid());
   for(i=0; i<sp; i++)
   printf("          ");
}
